﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Net.Http
{
    public class BizError: ApplicationException
    {
        private String code;
        private String msg;
        public BizError(String code, String message)
        {
            this.code = code;
            this.msg = message;
        }
        public String getCode()
        {
            return code;
        }

        public String getMsg()
        {
            return msg;
        }
    }
}
