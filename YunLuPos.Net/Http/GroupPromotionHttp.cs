﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;
using YunLuPos.Net.DTO;

namespace YunLuPos.Net.Http
{
    public class GroupPromotionHttp : HttpBase, GroupPromotionNetService
    {
        public List<GroupPromotion> list(PromotionVer maxVersion)
        {
            return doRequest<List<GroupPromotion>>(maxVersion, "pos/grouppromotion/list");
        }
    }
}
