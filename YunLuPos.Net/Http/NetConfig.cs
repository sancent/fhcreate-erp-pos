﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Net.Http
{
    public class NetConfig
    {
        public static int Timeout = 15000;
        public static int ReadWriteTimeout = 60000;
    }
}
