﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Net.Http;

namespace YunLuPos.Net
{
    public class NetServiceManager
    {
        public static GoodsNetService getGoodsNetService()
        {
            return new GoodsHttp();
        }

        public static SaleOrderNetService getSaleOrderNetService()
        {
            return new SaleOrderHttp();
        }

        public static CommonNetService getCommonNetService()
        {
            return new CommonHttp();
        }

        public static PromotionNetService getPromotionNetService()
        {
            return new PromotionHttp();
        }

        public static GroupPromotionNetService getGroupPromNetService()
        {
            return new GroupPromotionHttp();
        }

        public static MebNetService getMebNetService()
        {
            return new MebHttp();
        }
    }
}
