﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.Net.DTO
{
    public class PosCommInfo
    {
        public List<PayType> payTypes { get; set; }

        public List<Cashier> posCashier { get; set; }
    }
}
