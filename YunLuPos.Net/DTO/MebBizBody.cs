﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Net.DTO
{
    public class MebBizBody
    {
		public String cliqueCode { get; set; }

		public String branchCode { get; set; }

		public String posCode { get; set; }

		public String cahsierCode { get; set; }

		public String cardNum { get; set; }

		public Double amount { get; set; }

		public String orderCode { get; set; }

		public String flowCode { get; set; }


	}
}
