﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;
using YunLuPos.Net.DTO;

namespace YunLuPos.Net
{
    public interface GoodsNetService
    {
        /**
         * 根据商品唯一标识（服务器）查询商品列表
         * */
        List<Goods> list(List<long> ids);

        /**
         * 根据本地最新版本号查询服务器需要更新的商品唯一标识列表
         * */
        List<long> ids(GoodsInfoVer maxVersion);
    }
}
