﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class CommInfo
    {
        public String cliqueCode { get; set; }

        public String cliqueName { get; set; }

        public String shopCode { get; set; }

        public String shopName { get; set; }

        public String posCode { get; set; }

        public String posName { get; set; }

        public String cashierCode { get; set; }

        public String cashierName { get; set; }

    }
}
