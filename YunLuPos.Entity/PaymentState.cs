﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class PaymentState
    {
        //总计需要支付
        public Double needPay { get; set; }

        //已经支付
        public Double payed { get; set; }

        //差值
        public Double less { get; set; }

        //舍分丢弃
        public Double dot { get; set; }

        //找零
        public Double change { get; set; }
    }
}
