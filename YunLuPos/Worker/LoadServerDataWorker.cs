﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Net;
using YunLuPos.Net.DTO;
using YunLuPos.Net.Http;

namespace YunLuPos.Worker
{
    public class LoadServerDataWorker
    {
        BackgroundWorker worker = null;
        GoodsService goodsService = new GoodsService();
        CashierService cashierService = new CashierService();
        PayTypeService payTypeService = new PayTypeService();
        PromotionService promService = new PromotionService();
        GroupPromotionService groupPromService = new GroupPromotionService();
        GoodsNetService goodsNet = NetServiceManager.getGoodsNetService();
        CommonNetService commNet = NetServiceManager.getCommonNetService();
        PromotionNetService promNet = NetServiceManager.getPromotionNetService();
        GroupPromotionNetService groupPromNet = NetServiceManager.getGroupPromNetService();

        public void syncWithReport(BackgroundWorker worker)
        {
            this.worker = worker;
            try
            {
                loadCommInfo();
                loadGoodsInfo();
                loadPromotion();
                loadGroupPromotion();
                DynamicInfoHoder.netIsAlive = true;
            }
            catch(NetException ne)
            {
                reportMessage(100,"网络错误，同步终止！");
            }catch(BizError be)
            {
                reportMessage(100, be.getMsg());
            }
        }

        void reportMessage(int i , String message)
        {
            if(worker != null)
            {
                worker.ReportProgress(i, message);
            }
        }

        /// <summary>
        /// 从服务器加载商品信息
        /// 分页加载
        /// 根据本地版本号加载所有需要更新的商品id
        /// 前天处理分页根据分页id列表加载商品信息
        /// </summary>
        public void loadGoodsInfo()
        {

            reportMessage(20,"->开始同步商品");
            String maxVer = goodsService.maxVersion();
            GoodsInfoVer ver = new GoodsInfoVer();
            ver.ver = maxVer;
            ver.cliqueCode = StaticInfoHoder.commInfo.cliqueCode;
            ver.branchCode = StaticInfoHoder.commInfo.shopCode;
            ver.posCode = StaticInfoHoder.commInfo.posCode;
            List<long> ids = goodsNet.ids(ver);
            if(ids == null || ids.Count <= 0)
            {
                reportMessage(30,"商品信息同步完成");
                return;
            }
            int rows = 500;
            int pages = (ids.Count + rows - 1) / rows;
            for (int i = 1; i <= pages; i++)
            {
                reportMessage(20,"-->商品信息执行第" + i + "/" + pages + "次同步");
                List<long> temp = new List<long>();
                int start = i * rows - rows;
                if (i == pages)
                {   //最后一页取所有
                    temp.AddRange(ids.GetRange(start, ids.Count % rows));
                }
                else
                {
                    temp.AddRange(ids.GetRange(start, rows));
                }
                List<Goods> list = goodsNet.list(temp);
                if (list != null && list.Count > 0)
                {
                   Int64 count = goodsService.insertOrUpdate(list);
                }
            }
            reportMessage(30,"商品信息同步完成");
        }


        /// <summary>
        /// 从服务器中加载基础信息，
        /// 收银员信息
        /// 收银方式信息
        /// </summary>
        public void loadCommInfo()
        {
            reportMessage(10,"->同步基础信息");
            String cVer = cashierService.maxVersion();
            String pVer = payTypeService.maxVersion();
            PosCommVer ver = new PosCommVer();
            ver.cashierVer = cVer;
            ver.payTypeVer = pVer;
            ver.cliqueCode = StaticInfoHoder.commInfo.cliqueCode;
            ver.branchCode = StaticInfoHoder.commInfo.shopCode;
            ver.posCode = StaticInfoHoder.commInfo.posCode;
            PosCommInfo info = commNet.sync(ver);
            if(info != null)
            {
                if(info.payTypes != null && info.payTypes.Count > 0)
                {
                    payTypeService.insertOrUpdate(info.payTypes);
                }

                if(info.posCashier != null && info.posCashier.Count > 0)
                {
                    cashierService.insertOrUpdate(info.posCashier);
                }
            }
            reportMessage(20,"基础信息同步完成");
        }


        /// <summary>
        /// 从服务器中加载普通促销信息，
        /// </summary>
        public void loadPromotion()
        {
            reportMessage(40,"->同步普通促销信息");
            String maxVer = promService.maxVersion();
            PromotionVer ver = new PromotionVer();
            ver.ver = maxVer;
            ver.cliqueCode = StaticInfoHoder.commInfo.cliqueCode;
            ver.branchCode = StaticInfoHoder.commInfo.shopCode;
            ver.posCode = StaticInfoHoder.commInfo.posCode;
            List<Promotion> list = promNet.list(ver);
            if (list != null && list.Count >0)
            {
                promService.insertOrUpdate(list);
            }
            reportMessage(50,"普通促销信息同步完成");
        }

        /// <summary>
        /// 从服务器中加载组合促销信息，
        /// </summary>
        public void loadGroupPromotion()
        {
            reportMessage(80,"->同步组合促销信息");
            String maxVer = groupPromService.maxVersion();
            PromotionVer ver = new PromotionVer();
            ver.cliqueCode = StaticInfoHoder.commInfo.cliqueCode;
            ver.branchCode = StaticInfoHoder.commInfo.shopCode;
            ver.posCode = StaticInfoHoder.commInfo.posCode;
            ver.ver = maxVer;
            List<GroupPromotion> list = groupPromNet.list(ver);
            if (list != null && list.Count > 0)
            {
                groupPromService.insertOrUpdate(list);
            }
            reportMessage(100,"组合促销信息同步完成");
        }
    }
}
