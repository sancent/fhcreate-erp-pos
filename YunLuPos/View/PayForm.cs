﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Entity.Constant;
using YunLuPos.Payment;

namespace YunLuPos.View
{
    public partial class PayForm : BaseDialogForm
    {
        private PayTypeService payTypeService = new PayTypeService();
        private SaleOrderAccountService accountService = new SaleOrderAccountService();
        private SaleOrderService orderService = new SaleOrderService();
        private PayType currentPayType = null;
        private SaleOrder order = null;
        public PaymentState state = null; //当前单据支付状态数据

        public PayForm(SaleOrder order)
        {
            InitializeComponent();
            this.order = order;
        }

        private void PayForm_Load(object sender, EventArgs e)
        {
            this.payedGrid.ClearSelection();
            this.ActiveControl = this.amountInput;
            List<PayType> list = null;
            if (SaleOrderType.SALE.ToString().Equals(order.orderType))
            {
               list = payTypeService.list();
            }else if (SaleOrderType.REJECT.ToString().Equals(order.orderType))
            {
               list = payTypeService.listRefundType();
            }
            this.payTypeGrid.DataSource = list;
        }

        /**
         * 刷新显示支付状态
         * */
        public void displayState()
        {
            state = orderService.getPaymentState(order.orderCode,currentPayType.isLessCent);
            this.payAmount.Text = state.needPay.ToString("F2");
            this.payedAmount.Text = state.payed.ToString("F2");
            this.lessAmount.Text = state.less.ToString("F2");
            this.amountInput.Text = state.less.ToString("F2");
            this.amountInput.SelectAll();
            this.payedGrid.DataSource = accountService.list(order.orderCode);
        }

        private void amountInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            // 如果输入的不是退格和数字，则屏蔽输入
            if (!(e.KeyChar == 8 || e.KeyChar == 46 || (e.KeyChar >= 48 && e.KeyChar <= 57)))
            {
                e.Handled = true;
            }
        }

        private void baseButton1_Click(object sender, EventArgs e)
        {
            this.amountInput.Focus();
            pay();
        }

        private void baseButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void PayForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                this.payTypeGrid.moveUp();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                this.payTypeGrid.moveDown();
                e.Handled = true;
            }
            else if(e.KeyCode == Keys.Enter)
            {
                pay();
            }
        }

        

        /**
         * 支付方式切换
         * */
        private void payTypeGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            if(payTypeGrid.CurrentRow != null)
            {
                PayType paytype = payTypeGrid.CurrentRow.DataBoundItem as PayType;
                if (!paytype.Equals(currentPayType))
                {
                    this.message.Text = "*";
                    currentPayType = paytype;
                    displayState();
                }
            }
        }


        /***
         * 调用支付
         * */
        void pay()
        {
            //检查是否支付完成
            if (state.less < 0)
            {
                return;
            }
            if (state.less == 0)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
                return;
            }

            //检查输入金额
            String amtStr = this.amountInput.Text;
            Double inputAmount = 0;
            try
            {
                inputAmount = Math.Round(Double.Parse(amtStr), 2);
            }
            catch
            {

            }
            if (inputAmount == 0)
            {
                return;
            }
            //检查溢收
            if (SaleOrderType.SALE.ToString().Equals(order.orderType))
            {
                if (!"1".Equals(currentPayType.isOverFlow)){
                    if (inputAmount > state.less)
                    {
                        this.message.Text = "【"+currentPayType.payTypeName+"】不允许溢收";
                        return;
                    }
                }
            }else if (SaleOrderType.REJECT.ToString().Equals(order.orderType))
            {
                if(inputAmount > state.less)
                {
                    this.message.Text = "退货不允许溢退";
                    return;
                }
            }
            //执行支付
            Console.WriteLine("234324dopay");
            MessageBox.Show("23432432432");
            ResultMessage rm = PaymentManager.doPay(order, currentPayType, state, inputAmount);
            Console.WriteLine("234324dopay333");
            Console.WriteLine("====================");
            Console.WriteLine(rm.Code);
            if (ResultMessage.SUCCESS.Equals(rm.Code))
            {
                displayState();
                this.message.Text = "*";
            }
            else
            {
                this.message.Text = rm.ErrorMessage;
            }
        }

    }
}
