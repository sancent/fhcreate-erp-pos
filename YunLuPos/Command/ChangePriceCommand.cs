﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.SellView;

namespace YunLuPos.Command
{
    class ChangePriceCommand : CommandInterface
    {
        SaleOrderGoodsService orderGoodsService = new SaleOrderGoodsService();
        public void exec(Form handel, Cashier auther)
        {
            if (DynamicInfoHoder.currentOrder.goods == null || DynamicInfoHoder.currentOrder.goods.Count <= 0)
            {
                return;
            }
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                NumberInputDialog input = new NumberInputDialog("修改价格");
                if (input.ShowDialog() == DialogResult.OK)
                {
                    String number = input.number.Text;
                    Double n = Double.Parse(number);
                    n = Math.Round(n, 2);
                    if (n <= 0)
                    {
                        return;
                    }
                    SaleOrderGoods goods = mf.goodsGrid.CurrentRow.DataBoundItem as SaleOrderGoods;
                    if (goods != null)
                    {
                        Boolean b = orderGoodsService.changePrice(goods, n, auther);
                        if (b)
                        {
                            mf.displayCurrentOrder(); //刷新当前单据界面显示
                        }

                    }

                }
            }


            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                Double value = 0;
                if (smf.InputDoubleDialog(ref value, 2, true, "请输入价格", smf.Style))
                {
                    Double n = value;
                    n = Math.Round(n, 2);
                    if (n <= 0)
                    {
                        return;
                    }
                    SaleOrderGoods goods = smf.goodsGrid.CurrentRow.DataBoundItem as SaleOrderGoods;
                    if (goods != null)
                    {
                        Boolean b = orderGoodsService.changePrice(goods, n, auther);
                        if (b)
                        {
                            smf.displayCurrentOrder(); //刷新当前单据界面显示
                        }

                    }
                }
            }

        }

        public string getKey()
        {
            return Names.ChangePrice.ToString();
        }

        public bool needAuth()
        {
            return true;
        }
    }
}
