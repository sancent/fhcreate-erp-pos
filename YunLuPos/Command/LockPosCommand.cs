﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.Entity;
using YunLuPos.SellView;

namespace YunLuPos.Command
{
    class LockPosCommand : CommandInterface
    {
        public void exec(Form handel, Cashier auther)
        {
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                ConfirmDialog confirm = new ConfirmDialog("确认离开？");
                if (confirm.ShowDialog() == DialogResult.OK)
                {
                    mf.lockPos();
                }
            }

            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                
                if (smf.ShowAskDialog("锁定收银机", "确认离开?", smf.Style))
                {
                    smf.lockPos();
                }
            }
        }

        public string getKey()
        {
            return Names.LockPos.ToString();
        }

        public bool needAuth()
        {
            return false;
        }
    }
}
