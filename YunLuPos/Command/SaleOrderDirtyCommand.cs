﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.SellView;

namespace YunLuPos.Command
{
    class SaleOrderDirtyCommand : CommandInterface
    {
        SaleOrderService orderService = new SaleOrderService();
        public void exec(Form handel, Cashier auther)
        {
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                ConfirmDialog confirm = new ConfirmDialog("作废当前单据？");
                if(confirm.ShowDialog() == DialogResult.OK)
                {
                    Boolean b = orderService.makeDirty(DynamicInfoHoder.currentOrder);
                    if (b)
                    {
                        mf.createNewOrder(false); //创建新单据不刷新前一单信息
                    }
                }
            }

            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                if (smf.goodsGrid.CurrentRow == null)
                {
                    return;
                }
                if (smf.ShowAskDialog("整单作废", "确认作废当前单据?", smf.Style))
                {
                    Boolean b = orderService.makeDirty(DynamicInfoHoder.currentOrder);
                    if (b)
                    {
                        smf.createNewOrder(false); //创建新单据不刷新前一单信息
                    }
                }
            }
        }

        public string getKey()
        {
            return Names.SaleOrderDirty.ToString();
        }

        public bool needAuth()
        {
            return true;
        }
    }
}
