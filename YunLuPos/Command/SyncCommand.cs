﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Entity.Constant;
using YunLuPos.SellView;
using YunLuPos.View;

namespace YunLuPos.Command
{
    class SyncCommand : CommandInterface
    {
        public void exec(Form handel, Cashier auther)
        {
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                SyncForm form = new SyncForm();
                form.ShowDialog();
            }


            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                SellSyncForm syncForm = new SellSyncForm();
                syncForm.Style = smf.Style;
                syncForm.ShowDialog();
            }
        }

        public string getKey()
        {
            return Names.Sync.ToString();
        }

        public bool needAuth()
        {
            return false;
        }
    }
}
