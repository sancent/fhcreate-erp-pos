﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Entity.Constant;
using YunLuPos.SellView;

namespace YunLuPos.Command
{
    class CountLessCommand : CommandInterface
    {
        SaleOrderGoodsService orderGoodsService = new SaleOrderGoodsService();
        GoodsService goodsService = new GoodsService();
        GroupPromotionService groupPromService = new GroupPromotionService();
        public void exec(Form handel, Cashier auther)
        {
            if (DynamicInfoHoder.currentOrder.goods == null || DynamicInfoHoder.currentOrder.goods.Count <= 0)
            {
                return;
            }

            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                SaleOrderGoods goods = mf.goodsGrid.CurrentRow.DataBoundItem as SaleOrderGoods;
                if (!GoodsPriceSource.NORM.ToString().Equals(goods.priceSource))
                {
                    new ConfirmDialog("组合促销商品不可手动修改数量").ShowDialog();
                    return;
                }
                if(goods.goodsCount < 1)
                {
                    return;
                }
                    Double n = goods.goodsCount - 1;
                    if (goods != null)
                    {
                        Boolean b = orderGoodsService.changeCount(goods,n);
                        if (b)
                        {
                            //触发组合促销处理
                            if (GoodsPriceSource.NORM.ToString().Equals(goods.priceSource))
                            {
                                groupPromService.doGroupProm(DynamicInfoHoder.currentOrder);
                            }
                            mf.displayCurrentOrder(); //刷新当前单据界面显示
                        }

                    }
                
            }



            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                SaleOrderGoods goods = smf.goodsGrid.CurrentRow.DataBoundItem as SaleOrderGoods;
                if (smf.goodsGrid.CurrentRow == null)
                {
                    return;
                }
                if (goods.goodsCount < 1)
                {
                    return;
                }
                Double n = goods.goodsCount - 1;
                    n = Math.Round(n, 3);
                    if (n <= 0)
                    {
                        return;
                    }
                    if (goods != null)
                    {
                        Boolean b = orderGoodsService.changeCount(goods, n);
                        if (b)
                        {
                            //触发组合促销处理
                            if (GoodsPriceSource.NORM.ToString().Equals(goods.priceSource))
                            {
                                groupPromService.doGroupProm(DynamicInfoHoder.currentOrder);
                            }
                            smf.displayCurrentOrder(); //刷新当前单据界面显示
                        }
                    }
                
            }


        }

        public string getKey()
        {
            return Names.NumLess.ToString();
        }

        public bool needAuth()
        {
            return false;
        }
    }
}
