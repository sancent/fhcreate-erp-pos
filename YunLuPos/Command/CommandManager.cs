﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.View;

namespace YunLuPos.Command
{
    class CommandManager
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(CommandManager));
        static List<CommandInterface> commanders = new List<CommandInterface>();
        static CashierService cashierService = new CashierService();
        static CommandManager()
        {
            commanders.Add(new BarCodeFocusCommand());
            commanders.Add(new SaleOrderDirtyCommand());
            commanders.Add(new SingleDelCommand());
            commanders.Add(new ChangeCountCommand());
            commanders.Add(new ChangePriceCommand());
            commanders.Add(new PaymentCommand());
            commanders.Add(new HoldOrderCommand());
            commanders.Add(new TakeOrderCommand());
            commanders.Add(new RefundCommand());
            commanders.Add(new LockPosCommand());
            commanders.Add(new SyncCommand());
            commanders.Add(new WeightCommand());
            commanders.Add(new ShowShortBarCommand());
            commanders.Add(new OrderSelectCommand());
            commanders.Add(new OpenBoxCommand());
            commanders.Add(new CountPlusCommand());
            commanders.Add(new CountLessCommand());
            commanders.Add(new MemberSearchCommand());
        }

        /**
         * 执行具体命令
         * */
        public static void Exec(Form handel,String commanderKey)
        {
            commanders.ForEach(l =>
            {
                if (l.getKey().Equals(commanderKey))
                {
                    if (l.needAuth())
                    {
                        //纳入权限管理的命令进行授权验证
                        Cashier cashier = cashierService.doAuth(StaticInfoHoder.commInfo.cashierCode, "", commanderKey, false);
                        if(cashier != null)
                        {
                            l.exec(handel,cashier);
                        }
                        else
                        {
                            AuthForm authForm = new AuthForm(commanderKey);
                            if(authForm.ShowDialog() == DialogResult.OK && authForm.authCashier != null)
                            {
                                l.exec(handel,authForm.authCashier);
                            }
                        }
                    }
                    else
                    {
                        //普通命令无需权限验证
                        l.exec(handel,null);
                    }
                    return;
                }

            });
        }
    }
}
