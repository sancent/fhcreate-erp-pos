﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Entity.Constant;
using YunLuPos.SellView;

namespace YunLuPos.Command
{
    class RefundCommand : CommandInterface
    {
        private SaleOrderService orderService = new SaleOrderService();
        private SaleOrderAccountService accountService = new SaleOrderAccountService();
        public void exec(Form handel, Cashier auther)
        {
            if (DynamicInfoHoder.currentOrder == null)
            {
                return;
            }
            List<SaleOrderAccount> accounts = accountService.list(DynamicInfoHoder.currentOrder.orderCode);
            if(accounts != null && accounts.Count > 0)
            {
                new ConfirmDialog("存在收款记录不允许切换销售模式！").ShowDialog();
                return;
            }
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                String typeName = SaleOrderType.SALE.ToString().Equals(DynamicInfoHoder.currentOrder.orderType) ? "进入退货模式？" : "退出退货模式？";
                ConfirmDialog confirm = new ConfirmDialog(typeName);
                if(confirm.ShowDialog() == DialogResult.OK)
                {
                    bool b = orderService.changeOrderType(DynamicInfoHoder.currentOrder);
                    if (b)
                    {
                        mf.displayCurrentOrder(true, false);
                    }
                }
            }


            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                String typeName = SaleOrderType.SALE.ToString().Equals(DynamicInfoHoder.currentOrder.orderType) ? "进入退货模式？" : "退出退货模式？";
                if (smf.ShowAskDialog("退货模式", typeName, smf.Style))
                {
                    bool b = orderService.changeOrderType(DynamicInfoHoder.currentOrder);
                    if (b)
                    {
                        smf.displayCurrentOrder(true, false);
                    }
                }
            }

        }

        public string getKey()
        {
            return Names.Refund.ToString();
        }

        public bool needAuth()
        {
            return true;
        }
    }
}
