﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.SellView;
using YunLuPos.View;

namespace YunLuPos.Payment
{
    public class HisPayPayment : PaymentInterface
    {
        private CodesService codesService = new CodesService();
        private SaleOrderAccountService accountService = new SaleOrderAccountService();
        PayTypeService payTypeService = new PayTypeService();
        public string getKey()
        {
            return "HISPAY";
        }

        public ResultMessage doPay(SaleOrder order, PayType payType, PaymentState state, double inputAmount)
        {
            //扫码支付
            String outTradeNo = codesService.genPayOutTradeNo(order.orderCode);
            SellHisPayForm sacnForm = new SellHisPayForm(outTradeNo, inputAmount);
            DialogResult result = sacnForm.ShowDialog();
            if(result == DialogResult.OK)
            {
                String e1Msg = accountService.addHisPayAccount(order,sacnForm.result);
                if(e1Msg == null)
                {
                    return ResultMessage.createSuccess();
                }
                //返回错误码
                String errorMessage = accountService.addAccount(order, payType, state, inputAmount);
                if (errorMessage != null)
                {
                    return ResultMessage.createError(errorMessage);
                }
                else
                {
                    return ResultMessage.createSuccess();
                }
            }
            else
            {
                return ResultMessage.createError("扫码取消");
            }
        }

        public List<string> print()
        {
            return new List<string>();
        }
    }
}
