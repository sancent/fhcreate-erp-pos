﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.Payment
{
    public interface PaymentInterface
    {

        String getKey();

        ResultMessage doPay(SaleOrder order, PayType payType, PaymentState state, Double inputAmount);

        List<String> print();
    }
}
