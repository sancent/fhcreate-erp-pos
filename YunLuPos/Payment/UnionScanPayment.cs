﻿using MPOS.SERVICE.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using YunLuPos.DB.Service;
using YunLuPos.Entity;

namespace YunLuPos.Payment
{
    class UnionScanPayment : PaymentInterface
    {
        private SaleOrderAccountService accountService = new SaleOrderAccountService();
        [DllImport(@"\gmc\posinf.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "bankall")]
        public static extern int bankall(string request, byte[] response);

        public static int _bankall(string request, byte[] response)
        {
            return bankall(request, response);
        }

        public ResultMessage doPay(SaleOrder order, PayType payType, PaymentState state, double inputAmount)
        {
            string request = payInfoToString( order,  payType,  state,  inputAmount);
            Console.WriteLine(request);
            byte[] response = new byte[2048];

            int result = bankall(request, response);
            String s = Encoding.Default.GetString(response, 0, 2);
            String content = Encoding.Default.GetString(response);
            YunSfResult yunResult = stringToPayResult(response);
            if(yunResult != null)
            {
                if ("00".Equals(yunResult.code))
                {
                    //返回错误码
                    String errorMessage = accountService.addAccount(order, payType, state, inputAmount);
                    if (errorMessage != null)
                    {
                        return ResultMessage.createError(errorMessage);
                    }
                    else
                    {
                        return ResultMessage.createSuccess();
                    }
                }else
                {
                    return ResultMessage.createError(yunResult.errorMessage);
                }
            }
            return ResultMessage.createError("支付错误未返回结果");
        }

        public string getKey()
        {
            return "UNIONSCAN";
        }

        public List<string> print()
        {
            throw new NotImplementedException();
        }

        //云闪付接口输入字符串转换
        private String payInfoToString(SaleOrder order, PayType payType, PaymentState state, double inputAmount)
        {
            if (order == null || inputAmount <= 0)
            {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("02");   //应用类型
            sb.Append(StaticInfoHoder.commInfo.posCode.PadRight(8, ' '));
            sb.Append(StaticInfoHoder.commInfo.cashierCode.PadRight(8, ' '));
            sb.Append("");

            String amt = ((Int32)(inputAmount * 100)) + "";
            Console.WriteLine(amt);
            amt = amt.PadLeft(12, '0');
            sb.Append(amt);
            sb.Append("        "); //原日期
            sb.Append("".PadRight(12, ' '));
            sb.Append("".PadRight(6, ' '));
            sb.Append("1".PadRight(3, '0'));

            sb.Append("7600000000000000".PadRight(50, ' '));//付款码

            sb.Append("".PadRight(50, ' '));

            sb.Append(order.orderCode.PadRight(50, ' '));

            sb.Append("".PadRight(32, ' '));
            //sb.Append("206AD245F4D0ABF2BFFD079ABFF0A54A3B8EEF19B8CCA2F83055825541EAF414" + "44A95EC234CC690EC04AB73DF931BE38F8A039ED7E5D78DB617A937FDE527DBB");
            sb.Append("\"goods\":[]");
            return sb.ToString();
        }


        /// <summary>
        /// 云闪付接口支付字符串转换
        /// </summary>
        /// <param name="strOut"></param>
        /// <returns></returns>
        private YunSfResult stringToPayResult(byte[] response)
        {
            YunSfResult result = new YunSfResult();
            result.code = Encoding.Default.GetString(response, 0, 2);
            result.bankName = Encoding.Default.GetString(response, 2, 4);
            result.cardNo = Encoding.Default.GetString(response, 6, 20);
            result.pzh = Encoding.Default.GetString(response, 26, 6);
            result.amount = Encoding.Default.GetString(response, 32, 12);
            result.errorMessage = Encoding.Default.GetString(response, 44, 40);
            result.mchId = Encoding.Default.GetString(response, 84, 15);
            result.driverNo = Encoding.Default.GetString(response, 99, 8);
            result.seqNo = Encoding.Default.GetString(response, 107, 6);
            result.payDate = Encoding.Default.GetString(response, 113, 4);
            result.payTime = Encoding.Default.GetString(response, 117, 6);
            result.refdata = Encoding.Default.GetString(response, 123, 12);
            result.authcode = Encoding.Default.GetString(response, 135, 6);
            result.stdate = Encoding.Default.GetString(response, 141, 4);
            result.lrc = Encoding.Default.GetString(response, 145, 3);
            result.distcount = Encoding.Default.GetString(response, 148, 12);
            result.cardType = Encoding.Default.GetString(response, 160, 2);
            result.thirdPartyDiscountInstrution = Encoding.Default.GetString(response, 162, 200);
            result.thirdPartyName = Encoding.Default.GetString(response, 362, 50);
            result.unionMerchant = Encoding.Default.GetString(response, 412, 50);
            result.erpMerchant = Encoding.Default.GetString(response, 462, 50);
            return result;
        }
    }
}
