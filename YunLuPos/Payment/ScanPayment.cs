﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.SellView;
using YunLuPos.View;

namespace YunLuPos.Payment
{
    public class ScanPayment : PaymentInterface
    {
        private CodesService codesService = new CodesService();
        private SaleOrderAccountService accountService = new SaleOrderAccountService();
        PayTypeService payTypeService = new PayTypeService();
        public string getKey()
        {
            return "SCANCODE";
        }

        public ResultMessage doPay(SaleOrder order, PayType payType, PaymentState state, double inputAmount)
        {
            //扫码支付
            String outTradeNo = codesService.genPayOutTradeNo(order.orderCode);
            SellScanPayCodeForm sacnForm = new SellScanPayCodeForm(outTradeNo, inputAmount);
            DialogResult result = sacnForm.ShowDialog();
            if(result == DialogResult.OK)
            {
                Dictionary<String,Object> r = sacnForm.result;
                if (r.ContainsKey("PAY_WAY"))
                {
                    String type = r["PAY_WAY"].ToString();
                    payType = payTypeService.getTypeByKey(type);
                }
                //返回错误码
                String errorMessage = accountService.addAccount(order, payType, state, inputAmount);
                if (errorMessage != null)
                {
                    return ResultMessage.createError(errorMessage);
                }
                else
                {
                    return ResultMessage.createSuccess();
                }
            }else
            {
                return ResultMessage.createError("扫码取消");
            }
        }

        public List<string> print()
        {
            return new List<string>();
        }
    }
}
