﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MPOS.SERVICE.Entity
{
    public class YunSfResult
    {
        public String code { get; set; }

        public String bankName { get; set; }

        public String cardNo { get; set; }

        public String pzh { get; set; } //凭证号

        public String amount { get; set; }

        public String errorMessage{ get; set; }

        public String mchId { get; set; }

        public String driverNo { get; set; }

        public String seqNo { get; set; }

        public String payDate { get; set; }

        public String payTime { get; set; }

        public String refdata { get; set; } //交易参考号

        public String authcode { get; set; } //授权号

        public String stdate { get; set; }

        public String lrc { get; set; }

        public String distcount { get; set; }

        public String cardType { get; set; }

        public String thirdPartyDiscountInstrution { get; set; }

        public String thirdPartyName  { get; set; }

        public String unionMerchant { get; set; }

        public String erpMerchant { get; set; }


        public String payType { get; set; }

        public String queryResCode { get; set; }

        public String queryResDesc { get; set; }

        public String billQRCode { get; set; }

        public String billDate  { get; set; }

        public String status { get; set; }



    }
}
