﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using YunLuPos.Utils;

namespace YunLuPos.Printer
{
   public class FilePrinter : PrintBase
    {
        private string PRINT_FILE = "PRINT_FILE";
        public override bool initPrinter()
        {
            PRINT_FILE = InIUtil.ReadInIData(InIUtil.PrintSection,InIUtil.PrintFile);
            return true;
        }

        public override bool Dispose()
        {
            return true;
        }

        public override bool NewRow()
        {
            File.AppendAllText(PRINT_FILE + "/print.txt", String.Format("\r\n"), Encoding.Default);
            return true;
        }

        public override bool write(byte[] b)
        {
            File.AppendAllText(PRINT_FILE + "/print.txt", Encoding.Default.GetString(b,0,b.Length), Encoding.Default);
            return true;
        }

        public override bool open()
        {
            try
            {
                File.Delete(PRINT_FILE + "/print.txt");
            }
            catch
            {

            }
            return true;
        }
    }
}
