﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YunLuPos.SellView
{
    public partial class ExitForm : UIForm
    {
        public ExitForm()
        {
            InitializeComponent();
            btnShutDown.Style = UIStyle.Red;
            btnUpdate.Style = UIStyle.Gray;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            exit();
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            restart();
        }

        private void btnShutDown_Click(object sender, EventArgs e)
        {
            shutDown();
        }

        void exit()
        {
            String pwd = "";
            if (this.InputPasswordDialog(ref pwd, true, "请输入系统维护密码", this.Style))
            {
                if ("000000".Equals(pwd))
                {
                    System.Environment.Exit(0);
                }
                else
                {
                    this.ShowErrorTip("系统维护密码错误");
                }
            }
        }
        void restart()
        {
            //重启
            Process.Start("shutdown.exe", "-r -t 1");
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        void shutDown()
        {
            //关机
            Process.Start("shutdown.exe", "-s -t 1");
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        void update()
        {
            if (this.ShowAskDialog("执行更新将退出系统，确认执行？",this.Style))
            {
                Process.Start(Path.Combine(Application.StartupPath, "LiveUpdate.exe"), null);
                System.Environment.Exit(0);
            }
        }

        private void ExitForm_KeyDown(object sender, KeyEventArgs e)
        {
            //查询快捷键过滤回车
            if (e.KeyCode == Keys.D1 || e.KeyCode == Keys.NumPad1)
            {
                exit();
            }
            else if (e.KeyCode == Keys.D2 || e.KeyCode == Keys.NumPad2)
            {
                restart();
            }
            else if (e.KeyCode == Keys.D3 || e.KeyCode == Keys.NumPad3)
            {
                shutDown();
            }
            else if (e.KeyCode == Keys.D4 || e.KeyCode == Keys.NumPad4)
            {
                update();
            }
            
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            update();
        }
    }
}
