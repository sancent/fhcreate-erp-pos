﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using YunLuPos.Pay;

namespace YunLuPos.SellView
{
    public partial class SellScanPayCodeForm : UIForm
    {
        private String bizType = "1";//默认支付业务
        private Boolean canSelect = false;
        private Double amount = 0;
        private String outTradeNo = "";
        private String code;
        public Dictionary<String, Object> result = null;

        public SellScanPayCodeForm()
        {
            InitializeComponent();
        }

        public SellScanPayCodeForm(String outTradeNo, Double amount)
        {
            InitializeComponent();
            this.amount = amount;
            this.outTradeNo = outTradeNo;
            this.amountLabel.Text = amount.ToString("F2");
            this.orderCode.Text = outTradeNo;
            this.selectButton.Style = UIStyle.Red;
            this.selectButton.Visible = false;
            this.selectButton.Text = StaticInfoHoder.PAY_SELECT_KEY+"查询结果";
        }

        private void SellScanPayCodeForm_Load(object sender, EventArgs e)
        {
            this.ActiveControl = this.authCode;
            setPaying(false);
        }

        /**
         * 设置界面正在支付中
         * */
        public void setPaying(Boolean b)
        {
            if (b)
            {
                this.bizType = "1";
                this.progress.Visible = true;
                this.message.Text = "支付处理中,请稍后 . . .";
                this.authCode.ReadOnly = true;
            }
            else
            {
                this.progress.Visible = false;
                this.message.Text = "";
                this.authCode.ReadOnly = false;
            }
        }

        public void setSelecting()
        {
            this.bizType = "2";
            this.progress.Visible = true;
            this.message.Text = "查询处理中,请稍后 . . .";
            this.authCode.ReadOnly = true;
        }

        

        /**
         * 设置单据可查询状态
         * */
        public void showSelect(bool b)
        {
            this.canSelect = b;
            this.selectButton.Visible = b;
        }

        private void authCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            const char Delete = (char)8;
            const char Enter = (char)13;
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9') && e.KeyChar != Delete
                        && e.KeyChar != Enter)
            {
                e.Handled = true;
            }
            else if (e.KeyChar == Enter)
            {
                if(this.authCode.Text == null || "".Equals(this.authCode.Text))
                {
                    return;
                }
                if (!payWorker.IsBusy)
                {
                    this.message.Text = "*";
                    code = authCode.Text;
                    this.authCode.Text = "";
                    setPaying(true);
                    payWorker.RunWorkerAsync();
                }
            }
        }

        private void payWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //微信支付宝扫码付
            Dictionary<String, Object> objectMap = new Dictionary<string, object>();
            objectMap.Add("SHOP_CODE", StaticInfoHoder.commInfo.shopCode);
            objectMap.Add("DEVICE_INFO", StaticInfoHoder.commInfo.posCode);
            objectMap.Add("OPERATOR_ID", StaticInfoHoder.commInfo.cashierCode);
            objectMap.Add("BUSINESS_TYPE", bizType);
            objectMap.Add("OUT_TRADE_NO", outTradeNo);
            objectMap.Add("TOTAL_FEE", amount * 100);
            objectMap.Add("AUTH_CODE", code);
            Dictionary<String, object> result = PayClient.send(objectMap, null);
            e.Result = result;
            
        }

        private void payWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Dictionary<String, object> result = (Dictionary<String, object>)e.Result;
            if (result != null && result.ContainsKey(C.RESULT_CODE) && C.SUCCESS.Equals(result[C.RESULT_CODE]))
            {
                //支付成功
                this.DialogResult = DialogResult.OK;
                this.result = result;
                this.Close();
            }
            else
            {
                 setPaying(false);
                 showSelect(true);
                //支付失败
                 String failMsg = String.Format("错误码：{0}\r\n信  息:{1}", result[C.FAIL_CODE], result[C.RESULT_MSG]);
                 if (result.ContainsKey("PAY_WAY"))
                 {
                     failMsg = "通道码:" + result["PAY_WAY"] + "\r\n" + failMsg;
                 }
                 this.message.Text = failMsg;
            }
        }

        private void SellScanPayCodeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (payWorker.IsBusy)
            {
                e.Cancel = true;
            }
        }

        private void selectButton_Click(object sender, EventArgs e)
        {
            if (!payWorker.IsBusy)
            {
                setSelecting();
                payWorker.RunWorkerAsync();
            }
        }

        private void SellScanPayCodeForm_KeyDown(object sender, KeyEventArgs e)
        {
            //查询快捷键过滤回车
            if(e.KeyCode == Keys.Enter)
            {
                return;
            }
            if(e.KeyCode.ToString().Equals(StaticInfoHoder.PAY_SELECT_KEY) && !payWorker.IsBusy)
            {
                setSelecting();
                payWorker.RunWorkerAsync();
            }
        }

        private void cardReadThread_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void cardReadThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
    }
}
