﻿namespace YunLuPos.SellView
{
    partial class SellLoginFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SellLoginFrom));
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.uiButton14 = new Sunny.UI.UIButton();
            this.uiButton13 = new Sunny.UI.UIButton();
            this.uiButton12 = new Sunny.UI.UIButton();
            this.uiButton11 = new Sunny.UI.UIButton();
            this.uiButton10 = new Sunny.UI.UIButton();
            this.uiButton9 = new Sunny.UI.UIButton();
            this.uiButton8 = new Sunny.UI.UIButton();
            this.uiButton7 = new Sunny.UI.UIButton();
            this.uiButton6 = new Sunny.UI.UIButton();
            this.uiButton5 = new Sunny.UI.UIButton();
            this.uiButton4 = new Sunny.UI.UIButton();
            this.uiButton3 = new Sunny.UI.UIButton();
            this.uiButton2 = new Sunny.UI.UIButton();
            this.taskMessage = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.syncProcess = new Sunny.UI.UIProcessBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiButton1 = new Sunny.UI.UIButton();
            this.pwdInput = new Sunny.UI.UITextBox();
            this.codeInput = new Sunny.UI.UITextBox();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.downWorker = new System.ComponentModel.BackgroundWorker();
            this.uiPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiPanel1
            // 
            this.uiPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.uiPanel1.Controls.Add(this.panel1);
            this.uiPanel1.Controls.Add(this.taskMessage);
            this.uiPanel1.Controls.Add(this.uiLabel3);
            this.uiPanel1.Controls.Add(this.syncProcess);
            this.uiPanel1.Controls.Add(this.pictureBox1);
            this.uiPanel1.Controls.Add(this.uiLabel2);
            this.uiPanel1.Controls.Add(this.uiLabel1);
            this.uiPanel1.Controls.Add(this.uiButton1);
            this.uiPanel1.Controls.Add(this.pwdInput);
            this.uiPanel1.Controls.Add(this.codeInput);
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel1.Location = new System.Drawing.Point(221, 176);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(609, 339);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = null;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.uiButton14);
            this.panel1.Controls.Add(this.uiButton13);
            this.panel1.Controls.Add(this.uiButton12);
            this.panel1.Controls.Add(this.uiButton11);
            this.panel1.Controls.Add(this.uiButton10);
            this.panel1.Controls.Add(this.uiButton9);
            this.panel1.Controls.Add(this.uiButton8);
            this.panel1.Controls.Add(this.uiButton7);
            this.panel1.Controls.Add(this.uiButton6);
            this.panel1.Controls.Add(this.uiButton5);
            this.panel1.Controls.Add(this.uiButton4);
            this.panel1.Controls.Add(this.uiButton3);
            this.panel1.Controls.Add(this.uiButton2);
            this.panel1.Location = new System.Drawing.Point(304, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(274, 281);
            this.panel1.TabIndex = 18;
            // 
            // uiButton14
            // 
            this.uiButton14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton14.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton14.Location = new System.Drawing.Point(139, 208);
            this.uiButton14.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton14.Name = "uiButton14";
            this.uiButton14.Size = new System.Drawing.Size(60, 60);
            this.uiButton14.TabIndex = 35;
            this.uiButton14.Text = ".";
            // 
            // uiButton13
            // 
            this.uiButton13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton13.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton13.Location = new System.Drawing.Point(7, 208);
            this.uiButton13.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton13.Name = "uiButton13";
            this.uiButton13.Size = new System.Drawing.Size(126, 60);
            this.uiButton13.TabIndex = 34;
            this.uiButton13.Text = "0";
            this.uiButton13.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // uiButton12
            // 
            this.uiButton12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton12.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton12.Location = new System.Drawing.Point(205, 142);
            this.uiButton12.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton12.Name = "uiButton12";
            this.uiButton12.Size = new System.Drawing.Size(60, 126);
            this.uiButton12.TabIndex = 33;
            this.uiButton12.Text = "确认";
            // 
            // uiButton11
            // 
            this.uiButton11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton11.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton11.Location = new System.Drawing.Point(205, 10);
            this.uiButton11.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton11.Name = "uiButton11";
            this.uiButton11.Size = new System.Drawing.Size(60, 126);
            this.uiButton11.TabIndex = 32;
            this.uiButton11.Text = "<-";
            // 
            // uiButton10
            // 
            this.uiButton10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton10.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton10.Location = new System.Drawing.Point(139, 142);
            this.uiButton10.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton10.Name = "uiButton10";
            this.uiButton10.Size = new System.Drawing.Size(60, 60);
            this.uiButton10.TabIndex = 31;
            this.uiButton10.Text = "9";
            // 
            // uiButton9
            // 
            this.uiButton9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton9.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton9.Location = new System.Drawing.Point(73, 142);
            this.uiButton9.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton9.Name = "uiButton9";
            this.uiButton9.Size = new System.Drawing.Size(60, 60);
            this.uiButton9.TabIndex = 30;
            this.uiButton9.Text = "8";
            // 
            // uiButton8
            // 
            this.uiButton8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton8.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton8.Location = new System.Drawing.Point(7, 142);
            this.uiButton8.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton8.Name = "uiButton8";
            this.uiButton8.Size = new System.Drawing.Size(60, 60);
            this.uiButton8.TabIndex = 29;
            this.uiButton8.Text = "7";
            // 
            // uiButton7
            // 
            this.uiButton7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton7.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton7.Location = new System.Drawing.Point(139, 76);
            this.uiButton7.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton7.Name = "uiButton7";
            this.uiButton7.Size = new System.Drawing.Size(60, 60);
            this.uiButton7.TabIndex = 28;
            this.uiButton7.Text = "6";
            // 
            // uiButton6
            // 
            this.uiButton6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton6.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton6.Location = new System.Drawing.Point(73, 76);
            this.uiButton6.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton6.Name = "uiButton6";
            this.uiButton6.Size = new System.Drawing.Size(60, 60);
            this.uiButton6.TabIndex = 27;
            this.uiButton6.Text = "5";
            // 
            // uiButton5
            // 
            this.uiButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton5.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton5.Location = new System.Drawing.Point(7, 76);
            this.uiButton5.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton5.Name = "uiButton5";
            this.uiButton5.Size = new System.Drawing.Size(60, 60);
            this.uiButton5.TabIndex = 26;
            this.uiButton5.Text = "4";
            // 
            // uiButton4
            // 
            this.uiButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton4.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton4.Location = new System.Drawing.Point(139, 10);
            this.uiButton4.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton4.Name = "uiButton4";
            this.uiButton4.Size = new System.Drawing.Size(60, 60);
            this.uiButton4.TabIndex = 25;
            this.uiButton4.Text = "3";
            // 
            // uiButton3
            // 
            this.uiButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton3.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton3.Location = new System.Drawing.Point(73, 10);
            this.uiButton3.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.Size = new System.Drawing.Size(60, 60);
            this.uiButton3.TabIndex = 24;
            this.uiButton3.Text = "2";
            // 
            // uiButton2
            // 
            this.uiButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton2.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButton2.Location = new System.Drawing.Point(7, 10);
            this.uiButton2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(60, 60);
            this.uiButton2.TabIndex = 23;
            this.uiButton2.Text = "1";
            this.uiButton2.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // taskMessage
            // 
            this.taskMessage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.taskMessage.Location = new System.Drawing.Point(67, 106);
            this.taskMessage.Name = "taskMessage";
            this.taskMessage.Size = new System.Drawing.Size(206, 23);
            this.taskMessage.TabIndex = 17;
            this.taskMessage.Text = "商品档案";
            this.taskMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel3.Location = new System.Drawing.Point(29, 106);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(51, 23);
            this.uiLabel3.TabIndex = 16;
            this.uiLabel3.Text = "同步:";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // syncProcess
            // 
            this.syncProcess.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.syncProcess.Location = new System.Drawing.Point(30, 132);
            this.syncProcess.MinimumSize = new System.Drawing.Size(70, 23);
            this.syncProcess.Name = "syncProcess";
            this.syncProcess.Size = new System.Drawing.Size(243, 29);
            this.syncProcess.TabIndex = 15;
            this.syncProcess.Text = "0.0%";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::YunLuPos.Properties.Resources.fh;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(40, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(222, 70);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(27, 221);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(46, 23);
            this.uiLabel2.TabIndex = 7;
            this.uiLabel2.Text = "密码:";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(27, 163);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(47, 23);
            this.uiLabel1.TabIndex = 6;
            this.uiLabel1.Text = "工号:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiButton1
            // 
            this.uiButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiButton1.Location = new System.Drawing.Point(30, 288);
            this.uiButton1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(242, 29);
            this.uiButton1.TabIndex = 5;
            this.uiButton1.Text = "登录";
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // pwdInput
            // 
            this.pwdInput.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.pwdInput.FillColor = System.Drawing.Color.White;
            this.pwdInput.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.pwdInput.Location = new System.Drawing.Point(29, 248);
            this.pwdInput.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pwdInput.Maximum = 2147483647D;
            this.pwdInput.Minimum = -2147483648D;
            this.pwdInput.MinimumSize = new System.Drawing.Size(1, 1);
            this.pwdInput.Name = "pwdInput";
            this.pwdInput.Padding = new System.Windows.Forms.Padding(5);
            this.pwdInput.PasswordChar = '*';
            this.pwdInput.Size = new System.Drawing.Size(242, 29);
            this.pwdInput.TabIndex = 4;
            this.pwdInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pwdInput_KeyPress);
            this.pwdInput.Leave += new System.EventHandler(this.pwdInput_Leave);
            // 
            // codeInput
            // 
            this.codeInput.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.codeInput.FillColor = System.Drawing.Color.White;
            this.codeInput.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.codeInput.Location = new System.Drawing.Point(29, 190);
            this.codeInput.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.codeInput.Maximum = 2147483647D;
            this.codeInput.Minimum = -2147483648D;
            this.codeInput.MinimumSize = new System.Drawing.Size(1, 1);
            this.codeInput.Name = "codeInput";
            this.codeInput.Padding = new System.Windows.Forms.Padding(5);
            this.codeInput.Size = new System.Drawing.Size(242, 29);
            this.codeInput.TabIndex = 3;
            this.codeInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.codeInput_KeyPress);
            this.codeInput.Leave += new System.EventHandler(this.codeInput_Leave);
            // 
            // uiLabel5
            // 
            this.uiLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiLabel5.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(848, 657);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(149, 23);
            this.uiLabel5.TabIndex = 6;
            this.uiLabel5.Text = "富海云创软件V1.0";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // downWorker
            // 
            this.downWorker.WorkerReportsProgress = true;
            this.downWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.downWorker_DoWork);
            this.downWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.downWorker_ProgressChanged);
            this.downWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.downWorker_RunWorkerCompleted);
            // 
            // SellLoginFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::YunLuPos.Properties.Resources.b1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1000, 700);
            this.Controls.Add(this.uiLabel5);
            this.Controls.Add(this.uiPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "SellLoginFrom";
            this.Text = "富海云创收银台";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SellLoginFrom_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SellLoginFrom_FormClosed);
            this.Load += new System.EventHandler(this.SellLoginFrom_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SellLoginFrom_KeyDown);
            this.uiPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Sunny.UI.UIPanel uiPanel1;
        private Sunny.UI.UIButton uiButton1;
        private Sunny.UI.UITextBox pwdInput;
        private Sunny.UI.UITextBox codeInput;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Sunny.UI.UILabel taskMessage;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UIProcessBar syncProcess;
        private Sunny.UI.UILabel uiLabel5;
        private System.ComponentModel.BackgroundWorker downWorker;
        private System.Windows.Forms.Panel panel1;
        private Sunny.UI.UIButton uiButton10;
        private Sunny.UI.UIButton uiButton9;
        private Sunny.UI.UIButton uiButton8;
        private Sunny.UI.UIButton uiButton7;
        private Sunny.UI.UIButton uiButton6;
        private Sunny.UI.UIButton uiButton5;
        private Sunny.UI.UIButton uiButton4;
        private Sunny.UI.UIButton uiButton3;
        private Sunny.UI.UIButton uiButton2;
        private Sunny.UI.UIButton uiButton14;
        private Sunny.UI.UIButton uiButton13;
        private Sunny.UI.UIButton uiButton12;
        private Sunny.UI.UIButton uiButton11;
    }
}