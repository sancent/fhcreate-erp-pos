﻿namespace YunLuPos.SellView
{
    partial class ExitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new Sunny.UI.UIButton();
            this.btnRestart = new Sunny.UI.UIButton();
            this.btnShutDown = new Sunny.UI.UIButton();
            this.btnUpdate = new Sunny.UI.UIButton();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnExit.Location = new System.Drawing.Point(22, 51);
            this.btnExit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(272, 36);
            this.btnExit.TabIndex = 26;
            this.btnExit.Text = "1  退出";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnRestart
            // 
            this.btnRestart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRestart.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnRestart.Location = new System.Drawing.Point(22, 101);
            this.btnRestart.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(272, 36);
            this.btnRestart.TabIndex = 27;
            this.btnRestart.Text = "2  重启";
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // btnShutDown
            // 
            this.btnShutDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnShutDown.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnShutDown.Location = new System.Drawing.Point(22, 152);
            this.btnShutDown.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnShutDown.Name = "btnShutDown";
            this.btnShutDown.Size = new System.Drawing.Size(272, 36);
            this.btnShutDown.TabIndex = 28;
            this.btnShutDown.Text = "3  关机";
            this.btnShutDown.Click += new System.EventHandler(this.btnShutDown_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnUpdate.Location = new System.Drawing.Point(22, 199);
            this.btnUpdate.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(272, 36);
            this.btnUpdate.TabIndex = 29;
            this.btnUpdate.Text = "4  更新";
            this.btnUpdate.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // ExitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 249);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnShutDown);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.btnExit);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExitForm";
            this.Text = "请选择退出选项";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ExitForm_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIButton btnExit;
        private Sunny.UI.UIButton btnRestart;
        private Sunny.UI.UIButton btnShutDown;
        private Sunny.UI.UIButton btnUpdate;
    }
}