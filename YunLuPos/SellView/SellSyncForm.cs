﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using YunLuPos.Worker;

namespace YunLuPos.SellView
{
    public partial class SellSyncForm : UIForm
    {
        LoadServerDataWorker serverDataWorker = new LoadServerDataWorker();
        public SellSyncForm()
        {
            InitializeComponent();
        }

        private void syncWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            serverDataWorker.syncWithReport(syncWorker);
        }

        private void syncWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.taskMessage.Text = e.UserState.ToString();
            syncProcess.Value = e.ProgressPercentage;
        }

        private void syncWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
        }

        private void SellSyncForm_Load(object sender, EventArgs e)
        {
            syncWorker.RunWorkerAsync();
        }
    }
}
