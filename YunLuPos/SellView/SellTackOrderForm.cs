﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;

namespace YunLuPos.SellView
{
    public partial class SellTackOrderForm : UIEditForm
    {
        public SaleOrder selectedOrder = null;
        private SaleOrderService orderService = new SaleOrderService();
        public SellTackOrderForm()
        {
            InitializeComponent();
        }

        private void SellTackOrderForm_Load(object sender, EventArgs e)
        {
            this.ActiveControl = this.orderGrid;
            this.orderGrid.Focus();
            this.orderGrid.AutoGenerateColumns = false;
            this.goodsGrid.AutoGenerateColumns = false;
            this.orderGrid.DataSource = orderService.listHoderOrder();
        }

        /// 重写键盘响应事件。拦截回车解决表格回车下一行问题
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter && this.orderGrid.Focused)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
                return true;
            }else if(keyData == Keys.Escape)
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
                return true;
            }
            else
            {
                return false;
            }
        }



        private void orderGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            if (orderGrid.CurrentRow != null)
            {
                SaleOrder order = orderGrid.CurrentRow.DataBoundItem as SaleOrder;
                if (!order.orderCode.Equals(this.selectedOrder))
                {
                    this.selectedOrder = orderService.getFullOrder(order.orderCode);
                    this.goodsGrid.DataSource = selectedOrder.goods;
                }
            }
        }

        private void SellTackOrderForm_KeyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine("keydown");
            if (e.KeyCode == Keys.Up)
            {
                moveUp();
            }
            else if (e.KeyCode == Keys.Down)
            {
                moveDown();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        /**
        * 下移选中行
        * 到最后一行停止
        * */
        public void moveUp()
        {
            Console.WriteLine(orderGrid.CurrentRow.Index);
            if (orderGrid.RowCount == 0)
            {
                return;
            }
            if (orderGrid.CurrentRow.Index > 0)
            {
                int moveRow = orderGrid.CurrentRow.Index;
                Console.WriteLine(moveRow);
                orderGrid.CurrentCell = orderGrid.Rows[moveRow].Cells[0];
            }
        }

        /**
        * 上一选中行
        * 到第一行停止
        * */
        public void moveDown()
        {
            if (orderGrid.RowCount == 0)
            {
                return;
            }
            if (orderGrid.CurrentRow.Index < orderGrid.RowCount - 1)
            {
                int moveRow = orderGrid.CurrentRow.Index;
                orderGrid.CurrentCell = orderGrid.Rows[moveRow].Cells[0];
            }
        }
    }
}
