﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace YunLuPos.SellView
{
    public partial class SellCardPayForm : UIForm
    {
        private String bizType = "1";//默认支付业务
        private Boolean canSelect = false;
        private Double amount = 0;
        private String outTradeNo = "";
        private String code;
        public SellCardPayForm()
        {
            InitializeComponent();
        }

        public SellCardPayForm(String outTradeNo, Double amount)
        {
            InitializeComponent();
            this.amount = amount;
            this.outTradeNo = outTradeNo;
            this.amountLabel.Text = amount.ToString("F2");
            this.orderCode.Text = outTradeNo;
            this.selectButton.Style = UIStyle.Red;
            this.selectButton.Visible = false;
            this.selectButton.Text = StaticInfoHoder.PAY_SELECT_KEY + "查询结果";
        }

        /**
         * 设置界面正在支付中
         * */
        public void setPaying(Boolean b)
        {
            if (b)
            {
                this.bizType = "1";
                this.progress.Visible = true;
                this.message.Text = "支付处理中,请稍后 . . .";
                this.cardNo.ReadOnly = true;
            }
            else
            {
                this.progress.Visible = false;
                this.message.Text = "";
                this.cardNo.ReadOnly = false;
            }
        }

        public void setSelecting()
        {
            this.bizType = "2";
            this.progress.Visible = true;
            this.message.Text = "查询处理中,请稍后 . . .";
            this.cardNo.ReadOnly = true;
        }

        /**
        * 设置单据可查询状态
        * */
        public void showSelect(bool b)
        {
            this.canSelect = b;
            this.selectButton.Visible = b;
        }

        private void payWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(1000);
        }

        private void payWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            setPaying(false);
            showSelect(true);
        }

        private void SellCardPayForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (payWorker.IsBusy)
            {
                e.Cancel = true;
            }
        }

        private void selectButton_Click(object sender, EventArgs e)
        {
            if (!payWorker.IsBusy)
            {
                setSelecting();
                payWorker.RunWorkerAsync();
            }
        }

        private void SellCardPayForm_KeyDown(object sender, KeyEventArgs e)
        {
            //查询快捷键过滤回车
            if (e.KeyCode == Keys.Enter)
            {
                return;
            }
            if (e.KeyCode.ToString().Equals(StaticInfoHoder.PAY_SELECT_KEY) && !payWorker.IsBusy)
            {
                setSelecting();
                payWorker.RunWorkerAsync();
            }
        }

        private void cardNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            const char Delete = (char)8;
            const char Enter = (char)13;
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9') && e.KeyChar != Delete
                        && e.KeyChar != Enter)
            {
                e.Handled = true;
            }
            else if (e.KeyChar == Enter)
            {
                if (this.cardNo.Text == null || "".Equals(this.cardNo.Text))
                {
                    return;
                }
                this.cardPwd.Focus();
            }
        }

        private void cardPwd_KeyPress(object sender, KeyPressEventArgs e)
        {
            const char Delete = (char)8;
            const char Enter = (char)13;
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9') && e.KeyChar != Delete
                        && e.KeyChar != Enter)
            {
                e.Handled = true;
            }
            else if (e.KeyChar == Enter)
            {
                Console.WriteLine("paying");
                this.message.Text = "*";
                this.cardNo.Text = "";
                this.cardPwd.Text = "";
                setPaying(true);
                payWorker.RunWorkerAsync();
            }
        }

        private void SellCardPayForm_Load(object sender, EventArgs e)
        {
            this.ActiveControl = this.cardNo;
            setPaying(false);
        }
    }
}
