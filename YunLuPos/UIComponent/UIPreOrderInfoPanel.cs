﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;

namespace YunLuPos.UIComponent
{
    public partial class UIPreOrderInfoPanel : UserControl
    {
        public UIPreOrderInfoPanel()
        {
            InitializeComponent();
        }

        public void setOrderInfo(SaleOrder order)
        {
            if (order == null)
            {
                this.payAmount.Text = "0.00";
                this.goodsCount.Text = "0.00";
                this.changeAmount.Text = "0.00";
            }
            else
            {
                this.payAmount.Text = "" + order.payAmount;
                this.goodsCount.Text = "" + order.goodsCount;
                this.changeAmount.Text = "0.00";
                if (order.accounts != null)
                {
                    order.accounts.ForEach(l =>
                    {
                        if ("CHANGE".Equals(l.typeKey))
                        {
                            this.changeAmount.Text = l.amount.ToString("F2");
                            return;
                        }
                    });
                }
                //this.disAmount.Text = "" + order.disAmount;
            }
        }
    }
}
