﻿namespace YunLuPos.UIComponent
{
    partial class UIMemberInfoPanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.mebCode = new Sunny.UI.UILabel();
            this.mebName = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiPanel1
            // 
            this.uiPanel1.Controls.Add(this.mebCode);
            this.uiPanel1.Controls.Add(this.mebName);
            this.uiPanel1.Controls.Add(this.uiLabel2);
            this.uiPanel1.Controls.Add(this.uiLabel1);
            this.uiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(203, 108);
            this.uiPanel1.TabIndex = 0;
            this.uiPanel1.Text = null;
            // 
            // mebCode
            // 
            this.mebCode.BackColor = System.Drawing.Color.Transparent;
            this.mebCode.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.mebCode.Location = new System.Drawing.Point(49, 45);
            this.mebCode.Name = "mebCode";
            this.mebCode.Size = new System.Drawing.Size(150, 23);
            this.mebCode.TabIndex = 4;
            this.mebCode.Text = "无";
            this.mebCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mebName
            // 
            this.mebName.BackColor = System.Drawing.Color.Transparent;
            this.mebName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.mebName.Location = new System.Drawing.Point(47, 11);
            this.mebName.Name = "mebName";
            this.mebName.Size = new System.Drawing.Size(150, 23);
            this.mebName.TabIndex = 3;
            this.mebName.Text = "无";
            this.mebName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(4, 43);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(49, 23);
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "编号:";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(4, 11);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(49, 23);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "会员:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UIMemberInfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uiPanel1);
            this.Name = "UIMemberInfoPanel";
            this.Size = new System.Drawing.Size(203, 108);
            this.uiPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel mebCode;
        private Sunny.UI.UILabel mebName;
        public Sunny.UI.UIPanel uiPanel1;
    }
}
