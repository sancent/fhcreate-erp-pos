﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace YunLuPos.Pay
{
    class FileLoggerImpl : IMalomePayLogger
    {
        string path = null;
        public FileLoggerImpl(String path)
        {
            this.path = path;
            
        }

        public void debug(string log)
        {
            String time = String.Format("{0:yyyyMMddHHmmss}",DateTime.Now);
            File.AppendAllText(path,String.Format("\r\n[{0}]-{1}", time, log),Encoding.Default);
        }

        public bool isDebugEnable()
        {
            return true;
        }
    }
}
