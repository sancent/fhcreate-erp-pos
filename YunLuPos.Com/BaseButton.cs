﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YunLuPos.Com
{
    public partial class BaseButton : Button
    {
        public Color CustomBackColor { get; set; }
        public Color CustomMouseDownColor { get; set; }
        public BaseButton()
        {
            InitializeComponent();
        }

        public BaseButton(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        private void paint(object sender, PaintEventArgs e)
        {
            if (BackColor != CustomBackColor)
            {
                this.BackColor = CustomBackColor;
            }
            if (this.FlatAppearance.MouseDownBackColor != CustomMouseDownColor)
            {
                this.FlatAppearance.MouseDownBackColor = CustomMouseDownColor;
            }
        }
    }
}
