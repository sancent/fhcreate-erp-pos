﻿namespace YunLuPos.Com
{
    partial class NumberInputDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.baseButton1 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton2 = new YunLuPos.Com.BaseButton(this.components);
            this.number = new YunLuPos.Com.BaseTextInput(this.components);
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Size = new System.Drawing.Size(287, 41);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Size = new System.Drawing.Size(287, 146);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.number);
            this.panel3.Controls.Add(this.baseButton2);
            this.panel3.Controls.Add(this.baseButton1);
            this.panel3.Size = new System.Drawing.Size(287, 105);
            // 
            // baseButton1
            // 
            this.baseButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton1.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton1.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton1.ForeColor = System.Drawing.Color.White;
            this.baseButton1.Location = new System.Drawing.Point(136, 68);
            this.baseButton1.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton1.Name = "baseButton1";
            this.baseButton1.Size = new System.Drawing.Size(70, 30);
            this.baseButton1.TabIndex = 0;
            this.baseButton1.Text = "Enter确认";
            this.baseButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton1.UseVisualStyleBackColor = false;
            this.baseButton1.Click += new System.EventHandler(this.baseButton1_Click);
            // 
            // baseButton2
            // 
            this.baseButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton2.ForeColor = System.Drawing.Color.White;
            this.baseButton2.Location = new System.Drawing.Point(210, 68);
            this.baseButton2.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton2.Name = "baseButton2";
            this.baseButton2.Size = new System.Drawing.Size(70, 30);
            this.baseButton2.TabIndex = 1;
            this.baseButton2.Text = "Esc取消";
            this.baseButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton2.UseVisualStyleBackColor = false;
            this.baseButton2.Click += new System.EventHandler(this.baseButton2_Click);
            // 
            // number
            // 
            this.number.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.number.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.number.Location = new System.Drawing.Point(10, 23);
            this.number.Name = "number";
            this.number.Size = new System.Drawing.Size(270, 26);
            this.number.TabIndex = 2;
            this.number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.baseTextInput1_KeyPress);
            // 
            // NumberInputDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 150);
            this.Name = "NumberInputDialog";
            this.Text = "NumberInputDialog";
            this.Load += new System.EventHandler(this.NumberInputDialog_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumberInputDialog_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private BaseButton baseButton2;
        private BaseButton baseButton1;
        public BaseTextInput number;
    }
}