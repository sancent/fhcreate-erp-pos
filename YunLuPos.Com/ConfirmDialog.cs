﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YunLuPos.Com
{
    public partial class ConfirmDialog : BaseDialogForm
    {
        public ConfirmDialog()
        {
            InitializeComponent();
        }

        public ConfirmDialog(String msg)
        {
            InitializeComponent();
            this.message.Text = msg;
        }

        private void baseButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void baseButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        
    }
}
