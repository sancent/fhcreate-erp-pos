﻿namespace YunLuPos.Com
{
    partial class GoodsInfoPanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.unit = new YunLuPos.Com.BaseLabel(this.components);
            this.specs = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel3 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel6 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel4 = new YunLuPos.Com.BaseLabel(this.components);
            this.codeInput = new YunLuPos.Com.BaseTextInput(this.components);
            this.goodsName = new YunLuPos.Com.BaseLabel(this.components);
            this.price = new YunLuPos.Com.BaseLabel(this.components);
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(242, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1, 87);
            this.panel1.TabIndex = 7;
            // 
            // unit
            // 
            this.unit.AutoSize = true;
            this.unit.ForeColor = System.Drawing.Color.White;
            this.unit.Location = new System.Drawing.Point(142, 63);
            this.unit.Name = "unit";
            this.unit.Size = new System.Drawing.Size(17, 12);
            this.unit.TabIndex = 10;
            this.unit.Text = "无";
            // 
            // specs
            // 
            this.specs.AutoSize = true;
            this.specs.ForeColor = System.Drawing.Color.White;
            this.specs.Location = new System.Drawing.Point(49, 63);
            this.specs.Name = "specs";
            this.specs.Size = new System.Drawing.Size(17, 12);
            this.specs.TabIndex = 9;
            this.specs.Text = "无";
            // 
            // baseLabel3
            // 
            this.baseLabel3.AutoSize = true;
            this.baseLabel3.ForeColor = System.Drawing.Color.White;
            this.baseLabel3.Location = new System.Drawing.Point(95, 63);
            this.baseLabel3.Name = "baseLabel3";
            this.baseLabel3.Size = new System.Drawing.Size(41, 12);
            this.baseLabel3.TabIndex = 8;
            this.baseLabel3.Text = "单位：";
            // 
            // baseLabel6
            // 
            this.baseLabel6.AutoSize = true;
            this.baseLabel6.ForeColor = System.Drawing.Color.White;
            this.baseLabel6.Location = new System.Drawing.Point(6, 63);
            this.baseLabel6.Name = "baseLabel6";
            this.baseLabel6.Size = new System.Drawing.Size(41, 12);
            this.baseLabel6.TabIndex = 6;
            this.baseLabel6.Text = "规格：";
            // 
            // baseLabel4
            // 
            this.baseLabel4.AutoSize = true;
            this.baseLabel4.ForeColor = System.Drawing.Color.White;
            this.baseLabel4.Location = new System.Drawing.Point(6, 39);
            this.baseLabel4.Name = "baseLabel4";
            this.baseLabel4.Size = new System.Drawing.Size(41, 12);
            this.baseLabel4.TabIndex = 4;
            this.baseLabel4.Text = "品名：";
            // 
            // codeInput
            // 
            this.codeInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.codeInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.codeInput.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeInput.ForeColor = System.Drawing.Color.White;
            this.codeInput.Location = new System.Drawing.Point(8, 4);
            this.codeInput.Name = "codeInput";
            this.codeInput.Size = new System.Drawing.Size(151, 26);
            this.codeInput.TabIndex = 2;
            this.codeInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.baseTextInput1_KeyPress);
            // 
            // goodsName
            // 
            this.goodsName.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.goodsName.ForeColor = System.Drawing.Color.White;
            this.goodsName.Location = new System.Drawing.Point(49, 37);
            this.goodsName.Name = "goodsName";
            this.goodsName.Size = new System.Drawing.Size(186, 16);
            this.goodsName.TabIndex = 1;
            this.goodsName.Text = "无";
            this.goodsName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // price
            // 
            this.price.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.price.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.price.ForeColor = System.Drawing.Color.White;
            this.price.Location = new System.Drawing.Point(162, 6);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(81, 23);
            this.price.TabIndex = 0;
            this.price.Text = "0.00";
            this.price.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GoodsInfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.unit);
            this.Controls.Add(this.specs);
            this.Controls.Add(this.baseLabel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.baseLabel6);
            this.Controls.Add(this.baseLabel4);
            this.Controls.Add(this.codeInput);
            this.Controls.Add(this.goodsName);
            this.Controls.Add(this.price);
            this.Name = "GoodsInfoPanel";
            this.Size = new System.Drawing.Size(243, 87);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseLabel price;
        private BaseLabel goodsName;
        private BaseLabel baseLabel4;
        private BaseLabel baseLabel6;
        private System.Windows.Forms.Panel panel1;
        private BaseLabel baseLabel3;
        private BaseLabel specs;
        private BaseLabel unit;
        public BaseTextInput codeInput;
    }
}
