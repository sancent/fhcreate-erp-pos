﻿namespace YunLuPos.Com
{
    partial class PreOrderInfoPanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.changeAmount = new YunLuPos.Com.BaseLabel(this.components);
            this.goodsCount = new YunLuPos.Com.BaseLabel(this.components);
            this.payAmount = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel3 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel2 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel1 = new YunLuPos.Com.BaseLabel(this.components);
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(170, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1, 80);
            this.panel1.TabIndex = 6;
            // 
            // changeAmount
            // 
            this.changeAmount.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeAmount.ForeColor = System.Drawing.Color.White;
            this.changeAmount.Location = new System.Drawing.Point(59, 57);
            this.changeAmount.Name = "changeAmount";
            this.changeAmount.Size = new System.Drawing.Size(85, 23);
            this.changeAmount.TabIndex = 5;
            this.changeAmount.Text = "0.00";
            // 
            // goodsCount
            // 
            this.goodsCount.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goodsCount.ForeColor = System.Drawing.Color.White;
            this.goodsCount.Location = new System.Drawing.Point(59, 32);
            this.goodsCount.Name = "goodsCount";
            this.goodsCount.Size = new System.Drawing.Size(85, 23);
            this.goodsCount.TabIndex = 4;
            this.goodsCount.Text = "0.00";
            // 
            // payAmount
            // 
            this.payAmount.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payAmount.ForeColor = System.Drawing.Color.White;
            this.payAmount.Location = new System.Drawing.Point(60, 6);
            this.payAmount.Name = "payAmount";
            this.payAmount.Size = new System.Drawing.Size(85, 23);
            this.payAmount.TabIndex = 3;
            this.payAmount.Text = "0.00";
            // 
            // baseLabel3
            // 
            this.baseLabel3.AutoSize = true;
            this.baseLabel3.ForeColor = System.Drawing.Color.White;
            this.baseLabel3.Location = new System.Drawing.Point(4, 61);
            this.baseLabel3.Name = "baseLabel3";
            this.baseLabel3.Size = new System.Drawing.Size(59, 12);
            this.baseLabel3.TabIndex = 2;
            this.baseLabel3.Text = "上单找零:";
            // 
            // baseLabel2
            // 
            this.baseLabel2.AutoSize = true;
            this.baseLabel2.ForeColor = System.Drawing.Color.White;
            this.baseLabel2.Location = new System.Drawing.Point(4, 36);
            this.baseLabel2.Name = "baseLabel2";
            this.baseLabel2.Size = new System.Drawing.Size(59, 12);
            this.baseLabel2.TabIndex = 1;
            this.baseLabel2.Text = "上单数量:";
            // 
            // baseLabel1
            // 
            this.baseLabel1.AutoSize = true;
            this.baseLabel1.ForeColor = System.Drawing.Color.White;
            this.baseLabel1.Location = new System.Drawing.Point(4, 10);
            this.baseLabel1.Name = "baseLabel1";
            this.baseLabel1.Size = new System.Drawing.Size(59, 12);
            this.baseLabel1.TabIndex = 0;
            this.baseLabel1.Text = "上单合计:";
            // 
            // PreOrderInfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.changeAmount);
            this.Controls.Add(this.goodsCount);
            this.Controls.Add(this.payAmount);
            this.Controls.Add(this.baseLabel3);
            this.Controls.Add(this.baseLabel2);
            this.Controls.Add(this.baseLabel1);
            this.Name = "PreOrderInfoPanel";
            this.Size = new System.Drawing.Size(171, 80);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseLabel baseLabel1;
        private BaseLabel baseLabel2;
        private BaseLabel baseLabel3;
        private BaseLabel payAmount;
        private BaseLabel goodsCount;
        private BaseLabel changeAmount;
        private System.Windows.Forms.Panel panel1;
    }
}
