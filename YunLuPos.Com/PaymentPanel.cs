﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;

namespace YunLuPos.Com
{
    public partial class PaymentPanel : UserControl
    {
        public PaymentPanel()
        {
            InitializeComponent();
        }

        public void setOrderInfo(SaleOrder order)
        {
            if(order == null)
            {
                this.payAmount.Text = "0.00";
                this.goodsCount.Text = "0.00";
                this.disAmount.Text = "0.00";
            }else
            {
                this.payAmount.Text = order.payAmount.ToString("F2");
                this.goodsCount.Text = order.goodsCount.ToString("F2");
                this.disAmount.Text = order.disAmount.ToString("F2");
            }
        }
    }
}
