﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;

namespace YunLuPos.Com
{
    public partial class ShortBar : UserControl
    {
        public delegate void OptionTouchHandle(String value, ShortBar handel);

        public event OptionTouchHandle OptionTouchEvent;

        public ShortBar()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            String tag = (String)btn.Tag;
            OptionTouchEvent?.Invoke(tag, this);
        }

        public void setContent(List<KeyBoard> keyBoards)
        {
            if(keyBoards == null || keyBoards.Count <= 0)
            {
                this.Visible = false;
                return;
            }else
            {
                this.Visible = true;
                keyBoards.ForEach(l => {
                    if ("1".Equals(l.showTouch))
                    {
                        BaseButton btn = new BaseButton();
                        btn.Text = l.keyCode + l.commandName;
                        btn.Click += new System.EventHandler(this.button_Click);
                        btn.Tag = l.keyCode;
                        Padding pad = new Padding(0, 0, 0, 5);
                        btn.Margin = pad;
                        this.flowLayoutPanel1.Controls.Add(btn);
                    }
                });
                if(this.flowLayoutPanel1.Controls.Count <= 0)
                {
                    this.Visible = false;
                }
            }


        }
    }
}
