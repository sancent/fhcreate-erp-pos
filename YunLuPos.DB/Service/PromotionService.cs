﻿using SQLiteSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.DB.Service
{
    public class PromotionService
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(PromotionService));
        /**
         * 根据条码或商品编码获取商品
         * */
        public Promotion getPromPrice(String barCode)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    DateTime dt = DateTime.Now;
                    String currentDate = dt.ToString("yyyyMMdd");
                    String currentTime = dt.ToString("HHmm");
                    Promotion prom = db.SqlQuery<Promotion>(@"select *  from Promotion 
                                                    where iStatus = '1' 
                                                    and barCode = @barCode 
                                                    and startDate <= @cD and endDate >= @cD 
                                                    and 
                                                    ((startTime <= @cT and endTime >= @cT)
                                                        or 
                                                     (startTime ='0000' and endTime = '0000')
                                                    )
                                                    order by ver desc limit 1",
                                                     new { barCode = barCode ,cD = currentDate,cT=currentTime}
                                                    )
                                                   .SingleOrDefault();

                    if (prom != null)
                    {
                        return prom;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }



        /**
        * 批量新增或更新商品
        * */
        public Int64 insertOrUpdate(List<Promotion> proms)
        {
            if (proms == null || proms.Count <= 0)
            {
                return 0;
            }
            DBLocker.setLocker();
            SqlSugarClient db = null;
            try
            {
                using (db = SugarDao.GetInstance())
                {
                    db.BeginTran();
                    foreach (Promotion p in proms)
                    {
                        Promotion dbPromotion = db.Queryable<Promotion>()
                            .Where(it => it.promCode == p.promCode && it.barCode == p.barCode && it.goodsCode == p.goodsCode)
                            .SingleOrDefault();
                        if (dbPromotion != null)
                        {
                            db.Delete<Promotion>(it => it.promCode == p.promCode && it.barCode == p.barCode && it.goodsCode == p.goodsCode);
                        }

                        //压单操作



                        db.Update<Promotion>(new { iStatus = "0" }, it => it.barCode == p.barCode && it.goodsCode == p.goodsCode);

                        db.Insert<Promotion>(p);
                    }
                    db.CommitTran();
                }
            }
            catch (Exception ex)
            {
                db.RollbackTran();
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return proms.Count;
        }



        public String maxVersion()
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    Object obj = db.Queryable<Promotion>().Max(it => it.ver);
                    if (obj == null || "".Equals(obj.ToString()))
                    {
                        return "0";
                    }
                    else
                    {
                        return obj.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return "0";
        }
    }
}
