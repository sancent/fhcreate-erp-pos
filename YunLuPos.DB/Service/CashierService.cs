﻿using SQLiteSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.DB.Service
{
    public class CashierService
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(CashierService));

        public Cashier doLogin(String code,String pwd)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    Cashier cashier = db.Queryable<Cashier>()
                         .Where(it => it.cashierCode == code)
                         .SingleOrDefault();
                    String encryptPwd = DESUtils.encrypt(cashier.branchCode + cashier.cashierCode + pwd);
                    if(cashier != null && cashier.pwd.Equals(encryptPwd))
                    {
                        return cashier;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }

        /**
         * 收银主管授权
         * */
        public Cashier doAuth(String code, String pwd,String commanderKey,Boolean isAuthPwd)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    Cashier cashier = db.Queryable<Cashier>()
                       .Where(it => it.cashierCode == code )
                       .SingleOrDefault();
                    if(cashier != null)
                    {
                        String authString = DESUtils.decrypt(cashier.authString);
                        bool b = authString.Contains(commanderKey);
                        if (!b)
                        {
                            cashier = null;
                        }
                    }
                    if (isAuthPwd)
                    {
                        if (cashier != null)
                        {
                            String encryptPwd = DESUtils.encrypt(cashier.branchCode + cashier.cashierCode + pwd);
                            if (cashier.pwd.Equals(pwd))
                            {
                                return cashier;
                            }
                        }
                    }else
                    {
                        return cashier;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }

        /**
       * 批量新增或更新收银员
       * */
        public Int64 insertOrUpdate(List<Cashier> cashiers)
        {
            if (cashiers == null || cashiers.Count <= 0)
            {
                return 0;
            }
            DBLocker.setLocker();
            SqlSugarClient db = null;
            try
            {
                using (db = SugarDao.GetInstance())
                {
                    db.BeginTran();
                    foreach (Cashier c in cashiers)
                    {
                        Cashier dbType = db.Queryable<Cashier>()
                            .Where(it => it.cashierCode == c.cashierCode)
                            .SingleOrDefault();
                        if (dbType != null)
                        {
                            db.Delete<Cashier>(it => it.cashierCode == c.cashierCode);
                        }
                        db.Insert<Cashier>(c);
                    }
                    db.CommitTran();
                }
            }
            catch (Exception ex)
            {
                db.RollbackTran();
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return cashiers.Count;
        }


        public String maxVersion()
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    Object obj = db.Queryable<Cashier>().Max(it => it.ver);
                    if (obj == null || "".Equals(obj.ToString()))
                    {
                        return "0";
                    }
                    else
                    {
                        return obj.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return "0";
        }

    }
}
