﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace YunLuPos.DB.Utils
{
    public class UMFIC
    {
        [DllImport("umf.DLL", EntryPoint = "fw_init")]
        public static extern Int32 fw_init(Int16 port, Int32 baud);
        [DllImport("umf.DLL", EntryPoint = "fw_config_card")]
        public static extern Int32 fw_config_card(Int32 icdev, Byte flags);
        [DllImport("umf.DLL", EntryPoint = "fw_exit")]
        public static extern Int32 fw_exit(Int32 icdev);
        [DllImport("umf.Dll", EntryPoint = "fw_request")]
        public static extern Int32 fw_request(Int32 icdev, Byte _Mode, UInt32[] TagType);
        [DllImport("umf.DLL", EntryPoint = "fw_anticoll")]
        public static extern Int32 fw_anticoll(Int32 icdev, Byte _Bcnt, ulong[] _Snr);
        [DllImport("umf.DLL", EntryPoint = "fw_select")]
        public static extern Int32 fw_select(Int32 icdev, UInt32 _Snr, Byte[] _Size);
        [DllImport("umf.DLL", EntryPoint = "fw_card")]
        public static extern Int32 fw_card(Int32 icdev, Byte _Mode, ulong[] _Snr);
        [DllImport("umf.DLL", EntryPoint = "fw_load_key")]
        public static extern Int32 fw_load_key(Int32 icdev, Byte _Mode, Byte _SecNr, Byte[] _NKey);
        [DllImport("umf.DLL", EntryPoint = "fw_authentication")]
        public static extern Int32 fw_authentication(Int32 icdev, Byte _Mode, Byte _SecNr);
        [DllImport("umf.DLL", EntryPoint = "fw_read")]
        public static extern Int32 fw_read(Int32 icdev, Byte _Adr, Byte[] _Data);

        [DllImport("umf.dll", EntryPoint = "fw_read_hex")]
        public static extern Int16 fw_read_hex(Int32 icdev, Byte _Adr, StringBuilder _Data);

        [DllImport("umf.DLL", EntryPoint = "fw_write")]
        public static extern Int32 fw_write(Int32 icdev, Byte _Adr, Byte[] _Data);

        [DllImport("umf.dll", EntryPoint = "fw_write_hex")]
        public static extern Int16 fw_write_hex(Int32 icdev, Byte _Adr, string _Data);

        [DllImport("umf.DLL", EntryPoint = "fw_halt")]
        public static extern Int32 fw_halt(Int32 icdev);
        [DllImport("umf.DLL", EntryPoint = "fw_changeb3")]
        public static extern Int32 fw_changeb3(Int32 icdev, Byte _SecNr, Byte[] _KeyA, Byte[] _CtrlW, Byte _Bk,
                 Byte[] _KeyB);
        [DllImport("umf.DLL", EntryPoint = "fw_initval")]
        public static extern Int32 fw_initval(Int32 icdev, Byte _Adr, UInt32 _Value);
        [DllImport("umf.DLL", EntryPoint = "fw_increment")]
        public static extern Int32 fw_increment(Int32 icdev, Byte _Adr, UInt32 _Value);
        [DllImport("umf.DLL", EntryPoint = "fw_readval")]
        public static extern Int32 fw_readval(Int32 icdev, Byte _Adr, UInt32[] _Value);
        [DllImport("umf.DLL", EntryPoint = "fw_decrement")]
        public static extern Int32 fw_decrement(Int32 icdev, Byte _Adr, UInt32 _Value);
        [DllImport("umf.DLL", EntryPoint = "fw_restore")]
        public static extern Int32 fw_restore(Int32 icdev, Byte _Adr);
        [DllImport("umf.DLL", EntryPoint = "fw_transfer")]
        public static extern Int32 fw_transfer(Int32 icdev, Byte _Adr);
        [DllImport("umf.DLL", EntryPoint = "fw_beep")]
        public static extern Int32 fw_beep(Int32 icdev, UInt32 _Msec);
        [DllImport("umf.DLL", EntryPoint = "fw_getver")]
        public static extern Int32 fw_getver(Int32 icdev, byte[] buff);
        [DllImport("umf.DLL", EntryPoint = "fw_reset")]
        public static extern Int16 fw_reset(Int32 icdev, UInt16 _Msec);
        [DllImport("umf.DLL", EntryPoint = "hex_a")]
        public static extern void hex_a(ref Byte hex, Byte[] a, Int16 len);

        //Ultralight functions
        [DllImport("umf.dll", EntryPoint = "fw_request_ultralt")]
        public static extern Int32 fw_request_ultralt(Int32 icdev, Byte _Mode);
        [DllImport("umf.dll", EntryPoint = "fw_anticall_ultralt")]
        public static extern Int32 fw_anticall_ultralt(Int32 icdev, ulong[] _Snr);
        [DllImport("umf.dll", EntryPoint = "fw_select_ultralt")]
        public static extern Int32 fw_select_ultralt(Int32 icdev, ulong _Snr);
        [DllImport("umf.dll", EntryPoint = "fw_write_ultralt")]
        public static extern Int32 fw_write_ultralt(Int32 icdev, Byte iPage, Byte[] wdata);
        [DllImport("umf.dll", EntryPoint = "fw_read_ultralt")]
        public static extern Int32 fw_read_ultralt(Int32 icdev, Byte iPage, Byte[] rdata);
        [DllImport("umf.dll", EntryPoint = "fw_tag_authenPwd")]
        public static extern Int32 fw_tag_authenPwd(Int32 icdev, Byte[] bufPwd);

        //NFC Functions
        [DllImport("umf.dll", EntryPoint = "fw_NFC_FormatCard_WithText")]
        public static extern Int32 fw_NFC_FormatCard_WithText(Int32 icdev, Int32 tagType, String IANA, String strForW,
            Int32 textFormat, String strKey, Boolean fLock);
        [DllImport("umf.dll", EntryPoint = "fw_NFC_FormatCard_WithURI")]
        public static extern Int32 fw_NFC_FormatCard_WithURI(Int32 icdev, Int32 tagType, Byte uriType, String szURI, String strKey, Boolean fLock);
        [DllImport("umf.dll", EntryPoint = "fw_NFC_FormatCard_WithSmartPoster")]
        public static extern Int32 fw_NFC_FormatCard_WithSmartPoster(Int32 icdev, Int32 tagType, String IANA, String strText, Int32 textFormat,
            Byte uriType, String strURI, String strKey, Boolean fLock);
        [DllImport("umf.dll", EntryPoint = "fw_NFC_FormatCard_WithContact")]
        public static extern Int32 fw_NFC_FormatCard_WithContact(Int32 icdev, Int32 tagType, String strName, String szTellCall,
            String szTellWork, String szTelFax, String szTelHome, String szOrg, String szDepart, String szWorkAdr, String szEmail,
            String szURL, String szXQQ, String szWeibo, String szWeiXin, Int32 textFormat, String strKey, Boolean fLock);
        [DllImport("umf.dll", EntryPoint = "fw_NFC_ReadTag")]
        public static extern Int32 fw_NFC_ReadTag(Int32 icdev, Int32 tagType, Int32 textFormat, Int32[] pPayloadType, StringBuilder strOutData);

        /// <summary>
        /// 打开USB读卡设备
        /// 100 标识usb  其他值表示串口
        /// 返回设备句柄
        /// </summary>
        public static Int32 openUsbDev()
        {
            Int32 tmphdev = fw_init(100, 0);
            return tmphdev;
        }


        /// <summary>
        /// 关闭设备 返回0表示关闭成功
        /// </summary>
        /// <param name="devNum"></param>
        /// <returns></returns>
        public static Int32 closeUsbDev(Int32 devNum)
        {
            return fw_exit(devNum);
        }



        /// <summary>
        /// 寻卡操作
        /// </summary>
        /// <param name="devNum">设备句柄</param>
        /// <param name="cardnumber">获取到的卡号数组、模式0单卡操作该数组长度1</param>
        /// <returns>0 表示寻卡成功</returns>
        public static Int32 findCard(int devNum, ulong[] cardnumber)
        {
            return fw_card(devNum, 0, cardnumber);
        }

        /// <summary>
        /// 卡扇区验证
        /// </summary>
        /// <param name="devNum">设备句柄</param>
        /// <param name="sector">要验证的扇区号0-16</param>
        /// <returns></returns>
        public static Int32 cardAuth(int devNum, Byte sector)
        {
            //0 keya  4 keyb
            return fw_authentication(devNum, 0, sector);
        }


        /// <summary>
        /// 读取卡块信息
        /// </summary>
        /// <param name="devNum">设备句柄</param>
        /// <param name="address">块地址 0-63</param>
        /// <returns></returns>
        public static String cardRead(int devNum, Byte address)
        {
            StringBuilder r_data = new StringBuilder(300000);
            byte[] data = new byte[16];
            byte[] nulldata = new byte[16];


            //0 keya  4 keyb
            Int32 state = fw_read(devNum, address, data);
            if(state == 0)
            {
                if (ASCIIEncoding.Default.GetString(data).Equals(ASCIIEncoding.Default.GetString(nulldata)))
                {
                    return null;
                }
                return ASCIIEncoding.Default.GetString(data);// r_data.ToString();
            }
            return null;
        }

        public static string HexToStr(string mHex) // 返回十六进制代表的字符串
        {
            mHex = mHex.Replace(" ", "");
            if (mHex.Length <= 0) return "";
            byte[] vBytes = new byte[mHex.Length / 2];
            for (int i = 0; i < mHex.Length; i += 2)
                if (!byte.TryParse(mHex.Substring(i, 2), NumberStyles.HexNumber, null, out vBytes[i / 2]))
                    vBytes[i / 2] = 0;
            return ASCIIEncoding.Default.GetString(vBytes);
        } /* HexToStr */

        public static int beep(Int32 devNum,UInt32 time)
        {
           return fw_beep(devNum, time);
        }


    }
}
