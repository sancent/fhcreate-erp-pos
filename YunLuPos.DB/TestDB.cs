﻿using SQLiteSugar;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace YunLuPos.DB
{
    public class TestDB
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TestDB));

        public void testconn()
        {
            logger.Info("测试日志");
            DBLocker.setLocker();
            using (SQLiteConnection conn = new SQLiteConnection(Constant.DB_FILE))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;

                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    Console.WriteLine(conn);
                    DBLocker.release();
                }
            }
            
        }

        public void testSugarconn()
        {
            logger.Info("测试日志sugar");
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    db.ClassGenerating.CreateClassFiles(db, ("d:/TestModels"), "YunLuPos.Entity");
                    Console.WriteLine(db);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
        }

    }
}
