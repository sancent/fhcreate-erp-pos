﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PosSetting.Pages
{
    public partial class BalancePage : UserControl,BasePage
    {
        public BalancePage()
        {
            InitializeComponent();
            this.bDeptTextbox.Text = InIUtil.ReadInIData(InIUtil.BalanceSection, InIUtil.BalanceStart);
            this.bEndWith.Text = InIUtil.ReadInIData(InIUtil.BalanceSection, InIUtil.BalanceEnd);
            this.bCode13Combox.Text = InIUtil.ReadInIData(InIUtil.BalanceSection, InIUtil.Balance13);
            this.bCode18Combox.Text = InIUtil.ReadInIData(InIUtil.BalanceSection, InIUtil.Balance18);
        }

        public void save()
        {
            InIUtil.WriteInIData(InIUtil.BalanceSection, InIUtil.BalanceStart, this.bDeptTextbox.Text);
            InIUtil.WriteInIData(InIUtil.BalanceSection, InIUtil.BalanceEnd, this.bEndWith.Text);
            InIUtil.WriteInIData(InIUtil.BalanceSection, InIUtil.Balance13, this.bCode13Combox.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.BalanceSection, InIUtil.Balance18, this.bCode18Combox.SelectedItem.ToString());
            MessageBox.Show("保存成功");
        }

        public void test()
        {
            
        }
    }
}
