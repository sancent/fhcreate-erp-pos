﻿namespace PosSetting.Pages
{
    partial class DisplayPage
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comCheckBit = new System.Windows.Forms.ComboBox();
            this.comStopBit = new System.Windows.Forms.ComboBox();
            this.comBit = new System.Windows.Forms.ComboBox();
            this.comRate = new System.Windows.Forms.ComboBox();
            this.comPort = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.comCheckBit);
            this.groupBox1.Controls.Add(this.comStopBit);
            this.groupBox1.Controls.Add(this.comBit);
            this.groupBox1.Controls.Add(this.comRate);
            this.groupBox1.Controls.Add(this.comPort);
            this.groupBox1.Location = new System.Drawing.Point(14, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(239, 254);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "客显串口设置";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 205);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 14);
            this.label13.TabIndex = 9;
            this.label13.Text = "校验位:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 164);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 14);
            this.label12.TabIndex = 8;
            this.label12.Text = "停止位:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 124);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 14);
            this.label11.TabIndex = 7;
            this.label11.Text = "数据位:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 83);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 14);
            this.label10.TabIndex = 6;
            this.label10.Text = "波特率:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 14);
            this.label9.TabIndex = 5;
            this.label9.Text = "端口:";
            // 
            // comCheckBit
            // 
            this.comCheckBit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comCheckBit.FormattingEnabled = true;
            this.comCheckBit.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even",
            "Mark",
            "Space"});
            this.comCheckBit.Location = new System.Drawing.Point(98, 202);
            this.comCheckBit.Name = "comCheckBit";
            this.comCheckBit.Size = new System.Drawing.Size(110, 22);
            this.comCheckBit.TabIndex = 4;
            // 
            // comStopBit
            // 
            this.comStopBit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comStopBit.FormattingEnabled = true;
            this.comStopBit.Items.AddRange(new object[] {
            "0",
            "1",
            "1.5",
            "2"});
            this.comStopBit.Location = new System.Drawing.Point(98, 161);
            this.comStopBit.Name = "comStopBit";
            this.comStopBit.Size = new System.Drawing.Size(110, 22);
            this.comStopBit.TabIndex = 3;
            // 
            // comBit
            // 
            this.comBit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBit.FormattingEnabled = true;
            this.comBit.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.comBit.Location = new System.Drawing.Point(98, 120);
            this.comBit.Name = "comBit";
            this.comBit.Size = new System.Drawing.Size(110, 22);
            this.comBit.TabIndex = 2;
            // 
            // comRate
            // 
            this.comRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comRate.FormattingEnabled = true;
            this.comRate.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "1800",
            "2400",
            "4800",
            "7200",
            "9600",
            "14400",
            "19200",
            "38400",
            "57600",
            "115200",
            "230400",
            "460800",
            "921600"});
            this.comRate.Location = new System.Drawing.Point(98, 79);
            this.comRate.Name = "comRate";
            this.comRate.Size = new System.Drawing.Size(110, 22);
            this.comRate.TabIndex = 1;
            // 
            // comPort
            // 
            this.comPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPort.FormattingEnabled = true;
            this.comPort.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15"});
            this.comPort.Location = new System.Drawing.Point(98, 38);
            this.comPort.Name = "comPort";
            this.comPort.Size = new System.Drawing.Size(110, 22);
            this.comPort.TabIndex = 0;
            // 
            // DisplayPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "DisplayPage";
            this.Size = new System.Drawing.Size(589, 346);
            this.Load += new System.EventHandler(this.DisplayPage_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comCheckBit;
        private System.Windows.Forms.ComboBox comStopBit;
        private System.Windows.Forms.ComboBox comBit;
        private System.Windows.Forms.ComboBox comRate;
        private System.Windows.Forms.ComboBox comPort;
    }
}
