﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace PosSetting.Pages
{
    public partial class DisplayPage : UserControl,BasePage
    {
        SerialPort outSerialPort = null;
        public DisplayPage()
        {
            InitializeComponent();
            this.comPort.Text = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComPort);
            this.comRate.Text = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComRate);
            this.comBit.Text = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComBit);
            this.comStopBit.Text = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComStopBit);
            this.comCheckBit.Text = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComCheckBit);
        }

        public void save()
        {
            InIUtil.WriteInIData(InIUtil.DisplaySection, InIUtil.DisplayComPort, this.comPort.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.DisplaySection, InIUtil.DisplayComRate, this.comRate.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.DisplaySection, InIUtil.DisplayComBit, this.comBit.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.DisplaySection, InIUtil.DisplayComStopBit, this.comStopBit.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.DisplaySection, InIUtil.DisplayComCheckBit, this.comCheckBit.SelectedItem.ToString());
            MessageBox.Show("保存成功");
        }

        public void test()
        {
            initOutPort();
            write(Encoding.ASCII.GetBytes("123"));
            outSerialPort.Close();
        }

        void initOutPort()
        {
            outSerialPort = new SerialPort();
            String OUTCOM_PORT = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComPort);
            String OUTCOM_RATE = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComRate);
            String OUTCOM_BIT = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComBit);
            String OUTCOM_STOPBIT = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComStopBit);
            String OUTCOM_CHECKBIT = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComCheckBit);
            //设置参数
            outSerialPort.PortName = OUTCOM_PORT;
            outSerialPort.BaudRate = Int32.Parse(OUTCOM_RATE); //串行波特率
            outSerialPort.DataBits = Int32.Parse(OUTCOM_BIT); //每个字节的标准数据位长度
            outSerialPort.StopBits = StopBits.One; //设置每个字节的标准停止位数
            if ("1".Equals(OUTCOM_STOPBIT))
            {
                outSerialPort.StopBits = StopBits.One;
            }
            else if ("1.5".Equals(OUTCOM_STOPBIT))
            {
                outSerialPort.StopBits = StopBits.OnePointFive;

            }
            else if ("2".Equals(OUTCOM_STOPBIT))
            {
                outSerialPort.StopBits = StopBits.Two;
            }
            outSerialPort.Parity = Parity.None; //设置奇偶校验检查协议
            if ("Even".Equals(OUTCOM_CHECKBIT))
            {
                outSerialPort.Parity = Parity.Even;
            }
            else if ("Mark".Equals(OUTCOM_CHECKBIT))
            {
                outSerialPort.Parity = Parity.Mark;
            }
            else if ("Odd".Equals(OUTCOM_CHECKBIT))
            {
                outSerialPort.Parity = Parity.Odd;
            }
            else if ("Space".Equals(OUTCOM_CHECKBIT))
            {
                outSerialPort.Parity = Parity.Space;
            }
           
        }

        public bool write(byte[] b)
        {
            try
            {
                outSerialPort.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.WriteLine(outSerialPort.IsOpen);
            if (outSerialPort != null && outSerialPort.IsOpen)
            {
                try
                {
                    Console.WriteLine(b.Length);
                    outSerialPort.Write(b, 0, b.Length);
                }
                catch (Exception e)
                {
                    return false;
                }
                finally
                {
                    outSerialPort.Close();
                }
                return true;
            }
            else
            {
                return false;
            }

        }

        private void DisplayPage_Load(object sender, EventArgs e)
        {

        }
    }
}
