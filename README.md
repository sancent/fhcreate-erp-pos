# fhcreate-erp-pos

#### 介绍
富海云创零售ERP-PC收银端

#### 软件架构
技术：C#WinForm + SqlLite + SqliteSugar + SunnyUI
开发环境：VS2019社区版开发

#### 模块介绍
PosSetting       收银台设置
YunLuPos.Pay     扫码付模块
YunLuPos         主程序
YunLuPos.Com     通用组件
YunLuPos.DB      数据库操作
YunLuPos.Entity  实体类
YunLuPos.Net     网络请求

#### 系统展示
![输入图片说明](readme/%E5%9B%BE%E7%89%871.png)
![输入图片说明](readme/%E5%9B%BE%E7%89%872.png)
![输入图片说明](readme/%E5%9B%BE%E7%89%873.png)
![输入图片说明](readme/%E5%9B%BE%E7%89%876.png)
![输入图片说明](readme/%E5%9B%BE%E7%89%875.png)
![输入图片说明](readme/%E5%9B%BE%E7%89%874.png)
![输入图片说明](readme/%E5%9B%BE%E7%89%877.png)
